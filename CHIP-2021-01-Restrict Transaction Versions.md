# CHIP 2021-01 Restrict Transaction Version

## Summary

This proposal forbids transactions to be mined if they lie about their version number.

> Title: Restrict Transaction Version
> Date: 2021-01  
> Type: Consensus  
> Status: Draft  
> Version: 0.3  
> Last edit: 2021-03-11  
> Owner: Tom Zander  
> History: https://gitlab.com/bitcoin.cash/chips  

## Motivation and Benefits

The transaction-version field in the transaction object has at this time no consensus rule on its correctness.

The transaction version has mostly been kept unused and the version of an incoming transaction has not had much of an effect on selection of code-paths.

Should in the future we decide to introduce a new transaction format, deciding how to parse in incoming transaction will best be done based on the transaction version. At such a time it then would be required to reject transactions that are advertising the wrong transaction number.

To allow for less complex software during such a transition it will be highly beneficial to ensure correct transaction versioning earlier.

## Impact / Costs and Risks

The new rule does not restrict transactions that currently comply with the standardness rules. As a result the only change would be in full nodes and specifically the block-validation technology.

The impact on the rest of the ecosystem is expected to be zero.

This is a tightening of allowed transactions in a block and therefore can be a soft-fork. Author(s) recommend this to be scheduled for a future protocol upgrade.

## Proposal Description

Transactions choose their version based on capabilties and, in future, on how the transaction is structured. Transactions shall state in the version field the correct version for that choice. At the time of writing this CHIP that limits the allowed version numbers to 1 or 2.

Blocks that have transactions which violate this rule shall be deemed invalid.

# References:

Discussion on [bitcoincashresearch](https://bitcoincashresearch.org/t/173)

## Statements

**BigBlockIfTrue, BCHN developer.**  
> Reviewed, looks good to me. Recommend activation of this CHIP in the first network upgrade after May 2021. [source](https://bitcoincashresearch.org/t/173/7)

**Freetrader, BCHN release manager.**  
Quoting the supporting statement from BigBlockIfTrue:  
> Likewise [source](https://bitcoincashresearch.org/t/173/10)

**Andrew Stone, developer Bitcoin Unlimited**  
> I support this change, so BU does so unless a BUIP is explicitly raised against it. ([source](https://bitcoincashresearch.org/t/173/11))

## Copyright Notice

Copyright (C)  2021 Tom Zander  

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.

